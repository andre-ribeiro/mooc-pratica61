
import java.util.HashMap;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time t = new Time();
        Jogador j = new Jogador(1, "Fulano");
        t.addJogador("Goleiro", j);
        j = new Jogador(4, "Ciclano");
        t.addJogador("Atacante", j);
        j = new Jogador(10, "Beltrano");
        t.addJogador("Lateral", j);
        
        Time t2 = new Time();
        j = new Jogador(1, "Jose");
        t2.addJogador("Goleiro", j);
        j = new Jogador(7, "Joao");
        t2.addJogador("Atacante", j);
        j = new Jogador(15, "Mario");
        t2.addJogador("Lateral", j);
        
        System.out.println("Posição \tTime 1\t\tTime 2");
        HashMap<String, Jogador> hm1 = t.getJogadores();
        HashMap<String, Jogador> hm2 = t2.getJogadores();
        for(String s : t.getJogadores().keySet()) {
            System.out.println(s + " \t" + hm1.get(s) + " \t" + hm2.get(s));
        }
    }
}
